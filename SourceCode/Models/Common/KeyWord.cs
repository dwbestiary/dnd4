﻿using DnD4Calculator.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DnD4Calculator.Models
{
        public class KeyWord : DnDObject
        {

            [NotMapped]
            public DnDObject TargetObject { get; set; }
            public KeyWord()
            { }
            public KeyWord(DnDObject target, DnDObject parentObject, string name, DnDType type) :base( parentObject, name, null, type)
            {
            this.TargetObject = target;
            }        
        }
}
