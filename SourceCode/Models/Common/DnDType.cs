﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DnD4Calculator.Models.Common
{
    public class DnDType: DnDObject
    {
        /// <summary>
        /// Системное имя Объекта
        /// </summary>
        public string SysName { get; set; }
        
        public DnDType()
        {

        }

        public DnDType(string sysName)
        {
            this.SysName = SysName;
        }
    }
}
