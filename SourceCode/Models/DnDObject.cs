﻿using DnD4Calculator.Models.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DnD4Calculator.Models
{
    public class DnDObject : DbContext
    {
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Уникальный идентификатор Системы
        /// </summary>
        public Guid Id { get; set; }
        public DnDObject()
        {
            KeyWords = new List<KeyWord>();
        }

        public DnDObject (DnDObject parentObject, string name, List<KeyWord> keyWords, DnDType objectType)
        {
            this.Id = Guid.NewGuid();
            this.KeyWords = keyWords == null ? new List<KeyWord>() : keyWords;
            this.Name = name;
            this.ParentObject = parentObject;
            this.Type = objectType;

        }
        
        /// <summary>
        /// Ссылка на родительский объект
        /// </summary>
        [NotMapped]
        public DnDObject ParentObject { get; set; }
        
        [NotMapped]
        public List<KeyWord> KeyWords { get; set; }
        [NotMapped]
        public DnDType Type { get; set; }

    }
}
