﻿using System.Collections.Generic;

namespace DnD4Calculator.Models.Creatures.Beasts
{
    public class Monster : DnDObject
    {
        public Monster()
        {

        }
        public int Level { get; set; }

        public string Description { get; set; }


        //public IEnumerable<Tag> Roles { get; set; }
    }
}
