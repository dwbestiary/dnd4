﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DnD4Calculator.Models;
using DnD4Calculator.Models.Creatures.Beasts;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DnD4Calculator.Models.Common;
using System.Security.Cryptography.Xml;

namespace DnD4Calculator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private DnDObject Kettary;
        private List<DnDType> TypeContext;
       private DataContext db;
        public void InitDAO (DataContext context)
        {
            db = context;
        }


        public HomeController(DataContext context, ILogger<HomeController> logger)
        {
            _logger = logger;
            var type = new DnDType("SysName");
            var keywords = new DnDType("KeyWord");
            var battlefieldRoles = new DnDType("BattlefieldRole");
            var origin = new DnDType("Origin");
            keywords.ParentObject = type;
            battlefieldRoles.ParentObject = keywords;
            origin.ParentObject = keywords;
            Kettary = new DnDObject(null, "Кеттари", null, type);
            TypeContext = new List<DnDType>{type, keywords, battlefieldRoles, origin};
            db = context;
        }

        public async Task<IActionResult> Index()
        {
            var monsterType = new DnDType("Monster");
            
            string description = "Гоблины - это злобные предатлеьские существа, которым нравятся грабежи и жестокость. Они не большие и не сильные, " +
                "но могут представлять опасность, если навалятся всем скопом. Гоблины быстро размножаются, и могут жить в любых местах, от пещер и руин - до городской канализации. " +
                "Они совершают набеги и ограбления, забирая у жертв все, что может пригодиться.";
            
            var monster = new Monster
            {   Level = 1, 
                Name = "Гоблин Воин", 
                Description = description,
                ParentObject = Kettary,
                Type = monsterType
            };
            db.Monsters.Add(monster);
            db.SaveChanges();

             
            return View(await db.Monsters.ToListAsync());
        }

        
        [HttpPost]
        public async Task<IActionResult> Create(Monster user)
        {
            
            db.Monsters.Add(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
